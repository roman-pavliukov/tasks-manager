# How to start

## Commands
```
docker-compose up -d
docker exec -it tasks-manager_php_1 sh
```
Then in container next command. Use this command to generate actual schema.
At this step let's not use migrations, but just generate schema from annotations.
Let's say it's development stage, so no need to have migrations.
```
composer install
bin/console doctrine:schema:update --force
```
In source code you can see simple CRUD operations provided by API Platform. 
Code is structured as DDD, but since we have only 1 entity (no services, write repos etc.)
it's only dividing app on `Domain`, `Application` and `Structure` layers.

## Based on API platform
See [doc](https://api-platform.com/docs/distribution/). Api platform provides to us a boilerplate
for creating CRUD in just 1 click. Although this framework really easy to adapt to DDD structure.
It has many modules out of the box, but I did changes only in `api` module. You're interested in
`/api` folder.

## P.S.
I don't have too much time for creating this test-task using TDD and DDD. It's only test, 
and I am not going to spend on it more than 2-3 hours (actually spent near 2 hours).
