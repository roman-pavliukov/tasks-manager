<?php

namespace App\Domain\TasksManagement\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Infrastructure\TasksManagement\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column
     * @Assert\NotBlank
     */
    public string $number = '';

    /**
     * @var string
     *
     * @ORM\Column
     * @Assert\NotBlank
     */
    public string $assignee = '';

    /**
     * @var string
     *
     * @ORM\Column
     * @Assert\NotBlank
     */
    public string $description = '';

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    public int $storyPoints = 0;

    public function getId(): ?int
    {
        return $this->id;
    }
}
